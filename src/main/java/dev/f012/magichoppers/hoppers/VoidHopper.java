package dev.f012.magichoppers.hoppers;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.block.Hopper;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class VoidHopper implements IHopper {

	@Override
	public boolean acceptsItem(ItemStack item, Hopper hopper) {
		return !hopper.isLocked();
	}

	@Override
	public void handleItem(ItemStack item, Hopper hopper, Inventory source) {
		Inventory inv = hopper.getInventory();
		inv.addItem(item.clone());
		item.setAmount(0);

		if (inv.getItem(4) == null) return;

		ItemStack toRemove = inv.getItem(0);
		if (toRemove != null) {
			inv.remove(toRemove);
			hopper.getWorld().playSound(hopper.getLocation(), Sound.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 1f, .5f);
		}

		for (int n = 1; n < inv.getSize(); n++) {
			ItemStack itm = inv.getItem(n);
			if (itm == null) continue;
			inv.remove(itm);
			inv.setItem(n - 1, itm);
		}
	}

	@Override
	public boolean allowDrain(ItemStack item, Hopper hopper) {
		return false; // We ain't gonna drain nothing
	}

	@Override
	public ShapedRecipe getRecipe() {
		ItemStack item = HopperType.VOID_HOPPER.createItem(1);
		ShapedRecipe recipe = new ShapedRecipe(HopperType.VOID_HOPPER.getNamespacedKey(), item);
		recipe.shape(
				"I0I",
				"EFE",
				"IEI"
		);

		recipe.setIngredient('0', Material.AIR);
		recipe.setIngredient('I', Material.IRON_INGOT);
		recipe.setIngredient('E', Material.ENDER_PEARL);
		recipe.setIngredient('F', HopperType.FAST_HOPPER.getRecipeChoice());

		return recipe;
	}

}
