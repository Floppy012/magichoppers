package dev.f012.magichoppers.hoppers;

import dev.f012.magichoppers.utils.GeneralUtil;
import org.bukkit.Material;
import org.bukkit.block.Hopper;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class FilterHopper extends FastHopper {

	@Override
	public boolean acceptsItem(ItemStack item, Hopper hopper) {
		int slot = hopper.getInventory().first(item.getType());

		return super.acceptsItem(item, hopper) && slot != -1 && GeneralUtil.getHopperDestinationSpace(item, hopper) > 0;
	}

	@Override
	public void handleItem(ItemStack item, Hopper hopper, Inventory source) {
		Inventory dest = GeneralUtil.getHopperDestination(hopper);
		int space = GeneralUtil.getInventorySpace(item, dest);

		if (space >= item.getAmount()) {
			dest.addItem(item.clone());
			item.setAmount(0);
		} else {
			ItemStack cloned = item.clone();
			cloned.setAmount(space);
			dest.addItem(cloned);
			item.setAmount(item.getAmount() - cloned.getAmount());
		}
	}

	
	
	@Override
	public boolean allowDrain(ItemStack item, Hopper hopper) {
		return false;
	}

	@Override
	public ShapedRecipe getRecipe() {
		ItemStack item = HopperType.FILTER_HOPPER.createItem(1);
		ShapedRecipe recipe = new ShapedRecipe(HopperType.FILTER_HOPPER.getNamespacedKey(), item);

		recipe.shape(
				"OBO",
				"RFR",
				"DTD"
		);

		recipe.setIngredient('O', Material.OBSERVER);
		recipe.setIngredient('B', Material.IRON_BARS);
		recipe.setIngredient('R', Material.REDSTONE);
		recipe.setIngredient('F', HopperType.FAST_HOPPER.getRecipeChoice());
		recipe.setIngredient('D', Material.DIAMOND);
		recipe.setIngredient('T', Material.REDSTONE_TORCH);

		return recipe;
	}


}
