package dev.f012.magichoppers.hoppers;

import dev.f012.magichoppers.MagicHoppers;
import dev.f012.magichoppers.utils.MHRecipeChoice;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.meta.ItemMeta;

public enum HopperType {
	FILTER_HOPPER(new FilterHopper(), "§1§lFilter hopper", 100_300),
	VOID_HOPPER(new VoidHopper(), "§5§lVoid hopper", 100_301),
	FAST_HOPPER(new FastHopper(), "§6§lFast hopper", 100_302);

	public static final String MH_ITEM_NAME = "MagicHopper";

	@Getter private final IHopper handler;
	@Getter private final String hopperName;
	@Getter private final int modelData;
	@Getter private final NamespacedKey namespacedKey;
	@Getter private final RecipeChoice recipeChoice;

	private HopperType(IHopper handler, String hopperName, int modelData) {
		this.handler = handler;
		this.hopperName = hopperName;
		this.modelData = modelData;
		this.namespacedKey = new NamespacedKey(MagicHoppers.getInstance(), this.name().toLowerCase(Locale.ENGLISH));
		this.recipeChoice = new MHRecipeChoice(this);
	}

	public ItemStack createItem(int amount) {
		ItemStack stack = new ItemStack(Material.HOPPER, amount);
		ItemMeta meta = stack.getItemMeta();
		if (meta == null) return null;
		meta.setDisplayName(this.getHopperName());
		meta.setCustomModelData(this.getModelData());
		List<String> lore = new ArrayList<>();
		lore.add(MH_ITEM_NAME);
		meta.setLore(lore);
		stack.setItemMeta(meta);

		return stack;
	}

	public static boolean isMagicHopper(ItemStack item) {
		return getHopperTypeByItem(item) != null;
	}

	public static HopperType getHopperTypeByItem(ItemStack item) {
		if (item.getType() != Material.HOPPER || !item.hasItemMeta()) return null;

		ItemMeta meta = item.getItemMeta();
		if (meta == null) return null;

		List<String> lore = meta.getLore();

		if (lore == null || lore.size() != 1 || !meta.hasDisplayName()) return null;
		if (!lore.get(0).equals(MH_ITEM_NAME)) return null;

		HopperType hopperType = byModelData(meta.getCustomModelData());

		return hopperType;
	}

	public static HopperType byHopperName(String hopperName) {
		HopperType[] values = values();
		for (int n = 0; n < values.length; n++) {
			HopperType val = values[n];
			if (val.getHopperName().equals(hopperName)) return val;
		}

		return null;
	}

	public static HopperType byModelData(int modelData) {
		HopperType[] values = values();
		for (int n = 0; n < values.length; n++) {
			HopperType val = values[n];
			if (val.getModelData() == modelData) return val;
		}

		return null;
	}

	public static void registerRecipes() {
		HopperType[] values = values();
		for (int n = 0; n < values.length; n++) {
			HopperType val = values[n];
			Bukkit.getServer().addRecipe(val.getHandler().getRecipe());
		}
	}

}
