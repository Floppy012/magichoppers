package dev.f012.magichoppers.hoppers;

import dev.f012.magichoppers.utils.GeneralUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.World;
import org.bukkit.block.Hopper;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class FastHopper implements IHopper {

	@Override
	public boolean acceptsItem(ItemStack item, Hopper hopper) {
		Inventory dest = GeneralUtil.getHopperDestination(hopper);
		boolean special = true;
		
		if (dest != null && dest.getType() != InventoryType.HOPPER) {
			special = GeneralUtil.getInventorySpace(item, dest) > 0;
		}
		
		return !hopper.isLocked() && GeneralUtil.getInventorySpace(item, hopper.getInventory()) > 0 && special;
	}

	@Override
	public void handleItem(ItemStack item, Hopper hopper, Inventory source) {
		Inventory hopperDest = GeneralUtil.getHopperDestination(hopper);
		
		Inventory dest = hopper.getInventory();
		if (hopperDest != null && hopperDest.getType() != InventoryType.HOPPER && GeneralUtil.getInventorySpace(item, hopperDest) > 0) {
			dest = hopperDest;
		}
		
		int space = GeneralUtil.getInventorySpace(item, dest);

		if (space >= item.getAmount()) {
			dest.addItem(item.clone());
			item.setAmount(0);
		} else {
			ItemStack cloned = item.clone();
			cloned.setAmount(space);
			dest.addItem(cloned);
			item.setAmount(item.getAmount() - cloned.getAmount());
		}
		
		this.playSound(hopper.getLocation());
	}
	
	protected void playSound(Location loc) {
		World w = loc.getWorld();
		if (w == null) return;
		w.playSound(loc, Sound.ENTITY_PUFFER_FISH_BLOW_UP, SoundCategory.BLOCKS, 2f, 1f);
	}

	@Override
	public boolean allowDrain(ItemStack item, Hopper hopper) {
		return true;
	}

	@Override
	public ShapedRecipe getRecipe() {
		ItemStack item = HopperType.FAST_HOPPER.createItem(1);
		ShapedRecipe recipe = new ShapedRecipe(HopperType.FAST_HOPPER.getNamespacedKey(), item);
		recipe.shape(
				"W",
				"H",
				"M"
		);

		recipe.setIngredient('W', Material.WATER_BUCKET);
		recipe.setIngredient('H', Material.HOPPER);
		recipe.setIngredient('M', Material.MAGMA_BLOCK);

		return recipe;
	}

}
