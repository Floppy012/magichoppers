package dev.f012.magichoppers.hoppers;

import org.bukkit.block.Hopper;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public interface IHopper {

	public boolean acceptsItem(ItemStack item, Hopper hopper);

	public void handleItem(ItemStack item, Hopper hopper, Inventory source);

	public boolean allowDrain(ItemStack item, Hopper hopper);

	public ShapedRecipe getRecipe();

}
