package dev.f012.magichoppers;

import dev.f012.magichoppers.cmd.GiveCommand;
import dev.f012.magichoppers.cmd.MHCommandExecutor;
import dev.f012.magichoppers.hoppers.HopperType;
import dev.f012.magichoppers.listener.InventoryListener;
import dev.f012.magichoppers.listener.WorldListener;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

public class MagicHoppers extends JavaPlugin {

	@Getter private static MagicHoppers instance;

	@Override
	public void onEnable() {
		instance = this;
		HopperType.registerRecipes();
		this.getLogger().info("Hello World!");
		this.getServer().getPluginManager().registerEvents(new InventoryListener(), this);
		this.getServer().getPluginManager().registerEvents(new WorldListener(), this);
		this.registerCommands();
	}

	private void registerCommands() {
		MHCommandExecutor exec = new MHCommandExecutor();

		exec.registerCommand("give", new GiveCommand());

		this.getCommand("magichoppers").setExecutor(exec);
	}

}
