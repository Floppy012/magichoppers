package dev.f012.magichoppers.cmd;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class IMHCommand {

	@Getter private final String permission;
	@Getter private final boolean playerRequired;
	@Getter private final int minArgs;
	@Getter private final Map<Integer, List<String>> possibleArgs = new HashMap<>();

	final boolean handleCommand(CommandSender sender, String[] args) {
		if (this.permission != null && !sender.hasPermission(this.permission)) {
			sender.sendMessage("§cYou don't have permission to execute this command.");
			return true;
		}

		if (this.minArgs > -1 && args.length < this.minArgs) {
			return false;
		}

		if (this.playerRequired && !(sender instanceof Player)) {
			sender.sendMessage("§cYou need to be a player to execute this command!");
			return true;
		}

		return this.onCommand(sender, args);
	}

	protected abstract boolean onCommand(CommandSender sender, String[] args);

	final List<String> handleTabComplete(CommandSender sender, String[] args) {
		if (this.permission != null && !sender.hasPermission(this.permission)) {
			sender.sendMessage("§cYou don't have permission to use this command.");
			return null;
		}

		return this.onTabComplete(sender, args);
	}

	protected List<String> onTabComplete(CommandSender sender, String[] args) {
		int argNum = args.length - 1;
		if (!this.possibleArgs.containsKey(argNum)) return null;
		String arg = args[argNum];

		return arg.length() > 0 ? this.possibleArgs.get(argNum).stream().filter((a) -> a.startsWith(args[argNum])).collect(Collectors.toList()) : this.possibleArgs.get(argNum);
	}

	void printHelp(CommandSender sender) {
		sender.sendMessage("§7No additional help available ._.");
	}

	protected final void addPossibleArgs(int argNum, List<String> args) {
		this.possibleArgs.put(argNum, args);
	}

}
