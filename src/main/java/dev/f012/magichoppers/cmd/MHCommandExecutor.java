package dev.f012.magichoppers.cmd;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public class MHCommandExecutor implements CommandExecutor, TabCompleter {

	private final Map<String, IMHCommand> commands = new HashMap<>();

	@Override
	public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
		if (args.length == 0) return false;
		IMHCommand cmd = this.commands.get(args[0].toLowerCase(Locale.ENGLISH));
		if (cmd == null || args[0].equalsIgnoreCase("help")) {
			cs.sendMessage("§cUnknown command!");
			cs.sendMessage("§cPossible commands: /magichoppers <" + String.join("|", this.commands.keySet()) + "> [help|...args]");
			return true;
		}

		if (args.length > 1 && args[1].equalsIgnoreCase("help")) {
			cmd.printHelp(cs);
			return true;
		}

		boolean success = cmd.handleCommand(cs, this.filterCmdArg(args));

		if (!success) {
			cmd.printHelp(cs);
		}

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender cs, Command cmnd, String string, String[] args) {
		if (args.length > 1) {
			IMHCommand cmd = this.commands.get(args[0].toLowerCase(Locale.ENGLISH));
			if (cmd == null) return null;

			return cmd.onTabComplete(cs, this.filterCmdArg(args));
		}

		if (args.length != 1) return null;
		String arg = args[0];

		return arg.length() > 0
				? this.commands.keySet().stream().filter((k) -> k.startsWith(args[0])).collect(Collectors.toList())
				: this.commands.keySet().stream().collect(Collectors.toList());
	}

	public void registerCommand(String name, IMHCommand command) {
		this.commands.put(name, command);
	}

	private String[] filterCmdArg(String[] args) {
		String[] newArgs = new String[args.length - 1];
		System.arraycopy(args, 1, newArgs, 0, newArgs.length);

		return newArgs;
	}


}
