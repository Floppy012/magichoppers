package dev.f012.magichoppers.cmd;

import dev.f012.magichoppers.hoppers.HopperType;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GiveCommand extends IMHCommand {

	public GiveCommand() {
		super("magichoppers.give", true, 1);
		HopperType[] types = HopperType.values();
		List<String> args = new ArrayList<>();
		for (int n = 0; n < types.length; n++) {
			args.add(types[n].name().toLowerCase(Locale.ENGLISH));
		}

		this.addPossibleArgs(0, args);
	}

	@Override
	protected boolean onCommand(CommandSender sender, String[] args) {
		HopperType type;
		try {
			type = HopperType.valueOf(args[0].toUpperCase(Locale.ENGLISH));
		} catch (IllegalArgumentException e) {
			sender.sendMessage("§cThere is no hopper by that name!");
			sender.sendMessage("§cHopper: §6" + String.join(", ", this.getPossibleArgs().get(0)));
			return true;
		}

		int amount = 1;
		if (args.length > 1) {
			String amountStr = args[1];

			try {
				amount = Integer.parseInt(amountStr);
			} catch (NumberFormatException e) {
				sender.sendMessage("§4" + amountStr + "§c is not a valid number!");
				return false;
			}

			if (amount > 64 || amount < 1) {
				sender.sendMessage("§cThe amount has to be inbetween 1 and 64!");
				return true;
			}
		}

		Player player = (Player) sender;
		ItemStack item = type.createItem(amount);

		player.getInventory().addItem(item);
		return true;
	}

	@Override
	void printHelp(CommandSender sender) {
		sender.sendMessage("§cUsage: give <" + String.join("|", this.getPossibleArgs().get(0)) + "> [amount]");
	}


}
