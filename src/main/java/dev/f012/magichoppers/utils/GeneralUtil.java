package dev.f012.magichoppers.utils;

import dev.f012.magichoppers.config.DataFile;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Hopper;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GeneralUtil {

	public static int getHopperDestinationSpace(ItemStack item, Hopper hopper) {
		return getInventorySpace(item, GeneralUtil.getHopperDestination(hopper));
	}

	public static int getInventorySpace(ItemStack item, Inventory inv) {
		if (inv == null) return 0;

		if (inv.firstEmpty() == -1) {
			if (item.getMaxStackSize() == 1) return 0;
			Set<Integer> contItems = inv.all(item.getType()).keySet();
			int freeSpace = 0;

			for (int slot : contItems) {
				ItemStack stck = inv.getItem(slot);
				if (stck == null) continue;
				freeSpace += stck.getMaxStackSize() - stck.getAmount();
			}

			return freeSpace;
		}

		return item.getMaxStackSize();
	}

	public static Inventory getHopperDestination(Hopper hopper) {
		org.bukkit.block.data.type.Hopper typeHopper = (org.bukkit.block.data.type.Hopper) hopper.getBlockData();
		Block dest = hopper.getBlock().getRelative(typeHopper.getFacing());

		if (!(dest.getState() instanceof InventoryHolder)) return null;

		InventoryHolder destHolder = (InventoryHolder) dest.getState();
		Inventory inv = destHolder.getInventory();

		return inv;
	}

	public static Collection<Block> filterMagicHoppers(List<Block> blocks) {
		if (blocks.isEmpty()) return Collections.EMPTY_LIST;
		DataFile df = DataFile.getDataFile(blocks.get(0).getWorld());
		List<Block> toRemove = blocks.stream()
				.filter((b) -> b.getType() == Material.HOPPER)
				.filter((b) -> df.hasHopperAt(b.getX(), b.getY(), b.getZ()))
				.collect(Collectors.toList());
		blocks.removeAll(toRemove);
		return toRemove;
	}

}
