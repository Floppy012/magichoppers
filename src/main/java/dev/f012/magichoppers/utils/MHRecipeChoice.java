package dev.f012.magichoppers.utils;

import dev.f012.magichoppers.hoppers.HopperType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice.ExactChoice;

public class MHRecipeChoice extends ExactChoice {

	private final HopperType type;

	public MHRecipeChoice(HopperType type) {
		super(type.createItem(1));
		this.type = type;
	}

	@Override
	public boolean test(ItemStack is) {
		return HopperType.getHopperTypeByItem(is) == this.type;
	}

}
