package dev.f012.magichoppers.listener;

import dev.f012.magichoppers.config.DataFile;
import dev.f012.magichoppers.hoppers.HopperType;
import dev.f012.magichoppers.utils.GeneralUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;

public class WorldListener implements Listener {

	private static final Random RAND = new Random();
	private static final ArrayList<Block> BLOCKS_TO_REMOVE = new ArrayList<>();

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBlockPlace(BlockPlaceEvent event) {
		ItemStack itemPlaced = event.getItemInHand();
		Block blockPlaced = event.getBlockPlaced();
		HopperType hopperType = HopperType.getHopperTypeByItem(itemPlaced);
		if (hopperType == null) return;

		DataFile.getDataFile(event.getBlockPlaced().getWorld()).setHopperAt(blockPlaced.getX(), blockPlaced.getY(), blockPlaced.getZ(), hopperType);
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		if (block.getType() != Material.HOPPER) return;
		HopperType hopper = DataFile.getDataFile(block.getWorld()).getHopperAt(block.getX(), block.getY(), block.getZ());

		if (hopper == null) return;
		event.setDropItems(false);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBlockBreakMonitor(BlockBreakEvent event) {
		Block block = event.getBlock();
		if (block.getType() != Material.HOPPER) return;
		HopperType hopper = DataFile.getDataFile(block.getWorld()).removeHopperAt(block.getX(), block.getY(), block.getZ());

		if (hopper == null) return;
		if (event.getBlock().getDrops(event.getPlayer().getInventory().getItemInMainHand()).isEmpty()) return;
		block.getWorld().dropItemNaturally(block.getLocation(), hopper.createItem(1));
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockExplode(BlockExplodeEvent event) {
		this.filterExplosionBlocks(event.blockList());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBlockExplodeMonitor(BlockExplodeEvent event) {
		this.removeBlocksToRemove(false);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityExplode(EntityExplodeEvent event) {
		this.filterExplosionBlocks(event.blockList());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEnitityExplodeMonitor(EntityExplodeEvent event) {
		this.removeBlocksToRemove(event.getEntityType() == EntityType.PRIMED_TNT);
	}

	private void filterExplosionBlocks(List<Block> blocks) {
		Collection<Block> removed = GeneralUtil.filterMagicHoppers(blocks);
		BLOCKS_TO_REMOVE.addAll(removed);
	}

	private void removeBlocksToRemove(boolean isTnt) {
		if (BLOCKS_TO_REMOVE.isEmpty()) return;

		BLOCKS_TO_REMOVE.forEach((block) -> {
			HopperType hopper = DataFile.getDataFile(block.getWorld()).removeHopperAt(block.getX(), block.getY(), block.getZ());
			block.setType(Material.AIR);
			if (isTnt || RAND.nextInt(100) < 33) {
				block.getWorld().dropItemNaturally(block.getLocation(), hopper.createItem(1));
			}
		});

		BLOCKS_TO_REMOVE.clear();
	}
}
