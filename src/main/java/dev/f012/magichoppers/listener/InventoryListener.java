package dev.f012.magichoppers.listener;

import dev.f012.magichoppers.MagicHoppers;
import dev.f012.magichoppers.config.DataFile;
import dev.f012.magichoppers.hoppers.HopperType;
import dev.f012.magichoppers.hoppers.IHopper;
import dev.f012.magichoppers.utils.GeneralUtil;
import org.bukkit.Bukkit;
import org.bukkit.block.Hopper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class InventoryListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInventoryPickupItemEvent(InventoryPickupItemEvent event) {
		Inventory inv = event.getInventory();
		if (inv.getType() != InventoryType.HOPPER) return;
		InventoryHolder holder = inv.getHolder();

		if (!(holder instanceof Hopper)) return;
		Hopper hopper = (Hopper) holder;

		HopperType htype = DataFile.getDataFile(hopper.getWorld()).getHopperAt(hopper.getX(), hopper.getY(), hopper.getZ());
		if (htype == null) return;
		IHopper hopperHandler = htype.getHandler();

		event.setCancelled(true);
		if (!hopperHandler.acceptsItem(event.getItem().getItemStack(), hopper)) {
			return;
		}

		hopperHandler.handleItem(event.getItem().getItemStack(), hopper, null);

		if (event.getItem().getItemStack().getAmount() == 0) {
			event.getItem().remove();
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void preventAnvil(InventoryClickEvent event) {
		ItemStack item = event.getCurrentItem();
		Inventory clicked = event.getClickedInventory();
		Inventory inv = event.getInventory();

		if (clicked == null) return;

		if (event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY && clicked.getType() == InventoryType.PLAYER && inv.getType() == InventoryType.ANVIL) {
			if (HopperType.isMagicHopper(item)) {
				event.setCancelled(true);
				return;
			}
		}

		if (clicked.getType() == InventoryType.ANVIL) {
			switch (event.getAction()) {
				case PLACE_ALL:
				case PLACE_ONE:
				case PLACE_SOME:
				case SWAP_WITH_CURSOR:
					item = event.getCursor();
					if (HopperType.isMagicHopper(item)) {
						event.setCancelled(true);
					}
					break;
				default:
					break;
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void handleHopperDrain(InventoryMoveItemEvent event) {
		Inventory src = event.getSource();
		if (src.getType() != InventoryType.HOPPER) return;
		InventoryHolder srcHolder = src.getHolder();
	
		if (!(srcHolder instanceof Hopper)) return;
		Hopper srcHopper = (Hopper) srcHolder;
		DataFile df = DataFile.getDataFile(srcHopper.getWorld());
		
		HopperType srcType = df.getHopperAt(srcHopper.getX(), srcHopper.getY(), srcHopper.getZ());
		if (srcType == null) return;
		
		if (!srcType.getHandler().allowDrain(event.getItem(), srcHopper)) {
			event.setCancelled(true);
			return;
		}
		
		Inventory eventDest = event.getDestination();
		InventoryHolder eventDestHolder = eventDest.getHolder();
		
		if (!(eventDestHolder instanceof Hopper)) return;
		Hopper eventDestHopper = (Hopper) eventDestHolder;
		
		Inventory plannedDest = GeneralUtil.getHopperDestination(srcHopper); //Where the hopper points to
		
		if (plannedDest != null && plannedDest.getType() == InventoryType.HOPPER) {
			InventoryHolder plannedDestHolder = plannedDest.getHolder();
			
			if (plannedDestHolder instanceof Hopper) {
				Hopper plannedDestHopper = (Hopper) plannedDestHolder;
				HopperType plannedDestType = df.getHopperAt(plannedDestHopper.getX(), plannedDestHopper.getY(), plannedDestHopper.getZ());
				if (!eventDest.equals(plannedDest)) {
					if (plannedDestType != null && plannedDestType.getHandler().acceptsItem(event.getItem(), plannedDestHopper)) {
						event.setCancelled(true);
						return;
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void handleHopperLogic(InventoryMoveItemEvent event) {
		Inventory src = event.getSource();
		Inventory dst = event.getDestination();
		if (dst.getType() != InventoryType.HOPPER) return;
		InventoryHolder holder = dst.getHolder();

		if (!(holder instanceof Hopper)) return;
		Hopper hopper = (Hopper) holder;

		HopperType htype = DataFile.getDataFile(hopper.getWorld()).getHopperAt(hopper.getX(), hopper.getY(), hopper.getZ());
		if (htype == null) return;
		IHopper hopperHandler = htype.getHandler();

		event.setCancelled(true);
		if (!hopperHandler.acceptsItem(event.getItem(), hopper)) {
			return;
		}

		/*
			Unfortunately, someone at Spigot/Bukkit decided to set this event up in a way, that one cannot remove the moved item during the event process.
			The item is neither in the source inventory nor in the destination inventory. The setItem method doesn't allow to change the item. Otherwise
			the item won't be removed from the source inventory.
			There are several threads on the internet regarding this problem. This seems to be the most "efficient" solution. But IMHO it is a very ugly
			solution.
			~ Floppy
		 */
		Bukkit.getScheduler().runTaskLater(MagicHoppers.getInstance(), () -> {
			int srcSlot = src.first(event.getItem().getType());
			if (srcSlot == -1) return;
			ItemStack stack = src.getItem(srcSlot);
			if (stack == null) return;
			hopperHandler.handleItem(stack, hopper, dst);

			if (stack.getAmount() == 0) {
				src.setItem(srcSlot, null);
			}
		}, 1L);
	}

}
