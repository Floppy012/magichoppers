package dev.f012.magichoppers.config;

import com.google.gson.Gson;
import dev.f012.magichoppers.MagicHoppers;
import dev.f012.magichoppers.hoppers.HopperType;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.World;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DataFile {

	private static transient final Gson GSON = new Gson();
	private static transient final HashMap<String, DataFile> DATA_FILES = new HashMap<>();

	private final String worldName;

	private final HashMap<Long, MagicChunk> magicChunks = new HashMap<>();

	public boolean hasHopperAt(int x, int y, int z) {
		return this.getHopperAt(x, y, z) != null;
	}

	public HopperType getHopperAt(int x, int y, int z) {
		MagicChunk mc = this.getMagicChunkAtBlock(x, z, false);
		if (mc == null) return null;
		HopperData hopperData = mc.getChunkHopper(x & 15, y, z & 15);

		return hopperData == null ? null : hopperData.type;
	}

	public void setHopperAt(int x, int y, int z, HopperType type) {
		HopperType existingType = this.getHopperAt(x, y, z);
		if (existingType != null && existingType == type) return;
		MagicChunk mc = this.getMagicChunkAtBlock(x, z, true);
		mc.setChunkHopper(x & 15, y, z & 15, new HopperData(type, x, y, z));
		save();
	}

	public HopperType removeHopperAt(int x, int y, int z) {
		MagicChunk mc = this.getMagicChunkAtBlock(x, z, true);
		HopperData prevData = mc.setChunkHopper(x & 15, y, z & 15, null);

		if (mc.isEmpty()) this.removeMagicChunkAtBlock(x, z);
		save();

		return prevData != null ? prevData.type : null;
	}

	public void save() {
		File dataDir = MagicHoppers.getInstance().getDataFolder();
		if (!dataDir.exists()) {
			dataDir.mkdirs();
		}

		File dataFile = new File(dataDir, this.worldName + ".json");

		try (FileWriter writer = new FileWriter(dataFile)) {
			GSON.toJson(this, writer);
		} catch (IOException e) {
			throw new RuntimeException("Error while saving data!", e);
		}
	}

	private MagicChunk getMagicChunkAtBlock(int x, int z, boolean create) {
		int chunkX = x >> 4;
		int chunkZ = z >> 4;
		long combined = (long) chunkX << 32 | chunkZ & 0xFF_FF_FF_FFL;

		if (create) {
			return this.magicChunks.computeIfAbsent(combined, (comb) -> new MagicChunk(chunkX, chunkZ));
		} else {
			return this.magicChunks.get(combined);
		}
	}

	private void removeMagicChunkAtBlock(int x, int z) {
		int chunkX = x >> 4;
		int chunkZ = z >> 4;
		long combined = (long) chunkX << 32 | chunkZ & 0xFF_FF_FF_FFL;

		this.magicChunks.remove(combined);
	}

	public static DataFile load(World w) {
		File dataDir = MagicHoppers.getInstance().getDataFolder();
		File dataFile = new File(dataDir, w.getName() + ".json");
		DataFile df = null;
		if (!dataFile.exists()) {
			df = new DataFile(w.getName());
			df.save();
			DATA_FILES.put(w.getName(), df);
			return df;
		}

		try (FileReader reader = new FileReader(dataFile)) {
			df = GSON.fromJson(reader, DataFile.class);
		} catch (IOException e) {
			throw new RuntimeException("Error while loading data!", e);
		}

		DATA_FILES.put(w.getName(), df);

		return df;
	}

	public static DataFile getDataFile(World w) {
		return DATA_FILES.computeIfAbsent(w.getName(), (name) -> load(w));
	}

	@Getter
	@RequiredArgsConstructor
	private static class MagicChunk {

		private final int chunkX;
		private final int chunkZ;

		private final HashMap<Integer, HopperData> hoppers = new HashMap<>();

		private HopperData getChunkHopper(int inChunkX, int inChunkY, int inChunkZ) {
			int combined = inChunkY << 8 | inChunkX << 4 | inChunkZ;

			return this.hoppers.get(combined);
		}

		private HopperData setChunkHopper(int inChunkX, int inChunkY, int inChunkZ, HopperData data) {
			int combined = inChunkY << 8 | inChunkX << 4 | inChunkZ;

			if (data == null) {
				return this.hoppers.remove(combined);
			}

			this.hoppers.put(combined, data);
			return data;
		}

		private boolean isEmpty() {
			return this.hoppers.isEmpty();
		}

	}

	@RequiredArgsConstructor
	public static class HopperData {

		public final HopperType type;
		public final int blockX;
		public final int blockY;
		public final int blockZ;

	}


}
